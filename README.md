# 如何提高Windows下的pip安装下载速度

## 在 windows 命令提示符（默认C:\Users\Administrator>）中，
## 输入cd AppData\Roaming​​​，
## 进入此目录，在该目录下新建名为 pip 的文件夹：
## C:\Users\Administrator\AppData\Roaming>mkdir pip
## 然后在其中新建文件 pip.ini:
## C:\Users\Administrator\AppData\Roaming\pip>type nul>pip.ini
## 最后在文件管理器找到pip.ini用文本打开粘贴以下内容：
### 
### ----------------------------------------------------
### [global]
### index-url = http://pypi.douban.com/simple
### [install]
### trusted-host=pypi.douban.com
###  
### ----------------------------------------------------
## 
### 

### *************************************************
## 注：以下不是windows的粘贴内容！！！
#### 以下是linux的pip

#### $ vim ~/.pip/pip.conf 
#### [global]
#### index-url = https://pypi.tuna.tsinghua.edu.cn/simple
#### [install]
#### trusted-host=mirrors.aliyun.com

### 
#### linux国内源，如果windows的豆瓣源不能用，就尝试用下面的：
#### 清华：https://pypi.tuna.tsinghua.edu.cn/simple

#### 阿里云：http://mirrors.aliyun.com/pypi/simple/

#### 中国科技大学 https://pypi.mirrors.ustc.edu.cn/simple/

#### 华中理工大学：http://pypi.hustunique.com/

#### 山东理工大学：http://pypi.sdutlinux.org/ 

### *************************************************


